package th.ac.tu.siit.calculator;


import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity
	implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Button add = (Button)findViewById(R.id.add); 
        Button sub = (Button)findViewById(R.id.sub);
        Button mul = (Button)findViewById(R.id.mul);
        Button div = (Button)findViewById(R.id.div);
        Button bs = (Button)findViewById(R.id.bs);
        Button ac = (Button)findViewById(R.id.ac);
        Button dot = (Button)findViewById(R.id.dot);
        Button equ = (Button)findViewById(R.id.equ);
        
        Button zer = (Button)findViewById(R.id.num0);
        Button one = (Button)findViewById(R.id.num1);
        Button two = (Button)findViewById(R.id.num2);
        Button thr = (Button)findViewById(R.id.num3);
        Button fou = (Button)findViewById(R.id.num4);
        Button fiv = (Button)findViewById(R.id.num5);
        Button six = (Button)findViewById(R.id.num6);
        Button sev = (Button)findViewById(R.id.num7);
        Button eig = (Button)findViewById(R.id.num8);
        Button nin = (Button)findViewById(R.id.num9);
        
        
        add.setOnClickListener(this);
        sub.setOnClickListener(this);
        mul.setOnClickListener(this);
        div.setOnClickListener(this);
        bs.setOnClickListener(this);
        ac.setOnClickListener(this);
        dot.setOnClickListener(this);
        equ.setOnClickListener(this);
        
        zer.setOnClickListener(this);
        one.setOnClickListener(this);
        two.setOnClickListener(this);
        thr.setOnClickListener(this);
        fou.setOnClickListener(this);
        fiv.setOnClickListener(this);
        six.setOnClickListener(this);
        sev.setOnClickListener(this);
        eig.setOnClickListener(this);
        nin.setOnClickListener(this);
        
       
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    

    String output ="";
    String operand ="";
    String varOne="";
    double var1=0;
    double var2=0;
    double var3=0;
    @Override
    public void onClick(View v) {    
            
            
            int id = v.getId();
            //EditText input1 = (EditText)findViewById(R.id.Input1);
            //EditText input2 = (EditText)findViewById(R.id.Input2);
            //double var1 = Double.parseDouble(input1.getText().toString());
           // double var2 = Double.parseDouble(input2.getText().toString());
            TextView outputTV = (TextView)findViewById(R.id.output);
            TextView operandTV = (TextView)findViewById(R.id.operator);
            TextView var1TV = (TextView)findViewById(R.id.var1TV);
            
            switch(id) {
                case R.id.add:
                	if(operand.equals("+")){
                		if(output.equals("")){        			
                		}else{
	                		operand="+";
	                		var2=Double.parseDouble(output);
	                		var1=var1+var2;
	                		varOne=""+var1;
	                		output="";
                		}
                	}else if(operand.equals("BOOM")){              
                		var1=Double.parseDouble(output);  
                		varOne=""+var1;
                		output="";
                		operand="+";	
                	}else if(operand.equals("")){
                		var1=Double.parseDouble(output);
                		operand="+";
                		output="";
                		varOne=""+var1;
                	}else{
                		if(output.equals("")){                		
    	                }else{
    	                	if(operand=="+"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1+var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}else if(operand=="-"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1-var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}else if(operand=="x"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1*var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}else if(operand=="�"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1/var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}
                    	}	
                	
                		operand="+";	
                	}
                    break;
                case R.id.sub:
                	if(operand.equals("-")){
                		if(output.equals("")){        			
                		}else{
	                		operand="-";
	                		var2=Double.parseDouble(output);
	                		var1=var1-var2;
	                		varOne=""+var1;
	                		output="";
                		}
                	}else if(operand.equals("BOOM")){              
                		var1=Double.parseDouble(output);  
                		varOne=""+var1;
                		output="";
                		operand="-";	
                	}else if(operand.equals("")){
                		var1=Double.parseDouble(output);
                		operand="-";
                		output="";
                		varOne=""+var1;
                	}else{
                		if(output.equals("")){                		
    	                }else{
    	                	if(operand=="+"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1+var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}else if(operand=="-"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1-var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}else if(operand=="x"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1*var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}else if(operand=="�"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1/var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}
                    	}	
                	
                		operand="-";	
                	}
                    break;
                case R.id.mul:
                	if(operand.equals("x")){
                		if(output.equals("")){        			
                		}else{
	                		
	                		var2=Double.parseDouble(output);
	                		var1=var1*var2;
	                		varOne=""+var1;
	                		output="";
                		}
                	}else if(operand.equals("BOOM")){              
                		var1=Double.parseDouble(output);  
                		varOne=""+var1;
                		output="";
                			
                	}else if(operand.equals("")){
                		var1=Double.parseDouble(output);
                		
                		output="";
                		varOne=""+var1;
                	}else{
                		if(output.equals("")){                		
    	                }else{
    	                	if(operand=="+"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1+var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}else if(operand=="-"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1-var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}else if(operand=="x"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1*var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}else if(operand=="�"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1/var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}
                    	}	
                	}
                	operand="x";
                    break;

                case R.id.div:
                	if(operand.equals("�")){
                		if(output.equals("")){        			
                		}else{
	                		operand="�";
	                		var2=Double.parseDouble(output);
	                		var1=var1/var2;
	                		varOne=""+var1;
	                		output="";
                		}
                	}else if(operand.equals("BOOM")){              
                		var1=Double.parseDouble(output);  
                		varOne=""+var1;
                		output="";
                		operand="�";	
                	}else if(operand.equals("")){
                		var1=Double.parseDouble(output);
                		operand="�";
                		output="";
                		varOne=""+var1;
                	}else{
                		if(output.equals("")){                		
    	                }else{
    	                	if(operand=="+"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1+var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}else if(operand=="-"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1-var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}else if(operand=="x"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1*var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}else if(operand=="�"){
    	                		operand="BOOM";
    	                		var2=Double.parseDouble(output);
    	                		var1=var1/var2;
    	                		varOne=""+var1;
    	                		output="";
    	                	}
                    	}	
                	
               		operand="�";	
                	}
                    break;
               
                case R.id.equ:
                	if(output.equals("")){                		
	                }else{
	                	if(operand=="+"){
	                		operand="BOOM";
	                		var2=Double.parseDouble(output);
	                		var3=var1+var2;
	                		output=""+var3;
	                		varOne="";
	                	}else if(operand=="-"){
	                		operand="BOOM";
	                		var2=Double.parseDouble(output);
	                		var3=var1-var2;
	                		output=""+var3;
	                		varOne="";
	                	}else if(operand=="x"){
	                		operand="BOOM";
	                		var2=Double.parseDouble(output);
	                		var3=var1*var2;
	                		output=""+var3;
	                		varOne="";
	                	}else if(operand=="�"){
	                		operand="BOOM";
	                		var2=Double.parseDouble(output);
	                		var3=var1/var2;
	                		output=""+var3;
	                		varOne="";
	                	}
                	}
                    break;
                case R.id.ac:
                    outputTV.setText("0");
                    output="";
                    varOne="";
                    operand="";
                    break;
                case R.id.bs:
                	int l = output.length();
                	if(l>1){
                		output = output.substring(0,output.length()-1);
                	}else{
                		output="0";
                		
                	}
                	break;
                case R.id.dot:	
                	if(operand=="BOOM"){
                		output="";
                		varOne=""+var1;                		
                	}
                	output=output+((Button)v).getText().toString();
                        
                	
                	break;
                case R.id.num0:
                case R.id.num1:
                case R.id.num2:
                case R.id.num3:
                case R.id.num4:
                case R.id.num5:
                case R.id.num6:
                case R.id.num7:
                case R.id.num8:
                case R.id.num9:
                	if(operand=="BOOM"){
                		output="";
                		varOne=""+var1;                		
                	}
                	output=output+((Button)v).getText().toString();
                        
                    break;
            }
            var1TV.setText(varOne);	
            outputTV.setText(output);	
            operandTV.setText(operand);
            
            
        }
    
}
